
######################################################

library(quantmod)
getSymbols("^TWII",from="2001-01-01")
getSymbols("^SOX",from="2001-01-01")

plot_symbols = c("TWII","SOX")

op <- par(mfrow=c(2,2))
for (j in 1:length(plot_symbols)){
  chartSeries(
    get(plot_symbols[j]),
    layout=NULL,
#     major.ticks="years"
  )
}
par(op)


######################################################

pdf("Example6.pdf")
op <- par(mfrow=c(2,2))
for (j in 1:length(plot_symbols)){
  chartSeries(
    get(plot_symbols[j]),
    layout=NULL,
    #     major.ticks="years"
  )
}
par(op)
dev.off()

######################################################